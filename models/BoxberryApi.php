<?php

namespace app\models;

use Yii;

/**
 * Boxberry Api
 *
 * @property string $url Адрес API
 * @property string $token Токен
 * @author ilya
 */
class BoxberryApi extends \yii\base\Model {
    
    
    private static function get($methodName, $params = [])
    {
        $url = Yii::$app->params['boxberryApiUrl']."?token=".Yii::$app->params['boxberryApiToken']."&method=".$methodName;
        foreach ($params as $key => $val) {
            $url .= "&".urlencode($key)."=".urlencode($val);
        }
        try {
            $data = file_get_contents($url);
            $result = json_decode($data);
            return $result;
        } catch (Exception $ex) {
            // TODO: обработка ошибок
        }
    }
    
    public static function ListCities()
    {
        return static::get('ListCities');
    }
    
    public static function DeliveryCosts($weight, $target, $orderSum = 0)
    {
        return static::get('DeliveryCosts', [
            'weight' => $weight,
            'target' => $target,
            'ordersum' => $orderSum
        ]);
    }
}
