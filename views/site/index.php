<?php
/* @var $this yii\web\View */

use yii\helpers\Html,
    yii\helpers\ArrayHelper;

$this->title = 'Boxberry Calculator';
?>
<div class="site-index">
    <div class="body-content">
        <div class="row">
            <div class="col-lg-4">
                <?= Html::beginForm(['site/calculate'], 'get') ?>
                    <div class="form-group">
                        <label for="target">Город назначения</label>
                        <?=Html::dropDownList('target', 'target', ArrayHelper::map($cities, 'Code', 'Name'), ['id' => 'target', 'class' => 'form-control'])?>
                    </div>
                    <div class="form-group">
                        <label for="weight">Вес товара</label>
                        <?= Html::input('text', 'weight', '500', ['id' => 'weight', 'class' => 'form-control']) ?>
                        <p class="help-block">(в граммах)</p>
                    </div>
                    <div class="form-group">
                        <label for="orderSum">Стоимость товара</label>
                        <?= Html::input('text', 'orderSum', '0', ['id' => 'orderSum', 'class' => 'form-control']) ?>
                    </div>
                    <?= Html::submitButton('Рассчитать', ['class'=>'btn btn-default']) ?>
                <?= Html::endForm() ?>
            </div>
        </div>
    </div>
</div>
