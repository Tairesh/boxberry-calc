<?php
/* @var $this yii\web\View */
/* @var $price StdClass */

$this->title = 'Calculate result';
?>
<div class="site-calculate">
    <div class="body-content">
        <div class="row">
            <div class="col-lg-4">
                <h3>Результат:</h3>
                <p><strong>Стоимость:</strong> <?=$price->price?> руб.</p>
                <p><strong>Срок доставки:</strong> <?=$price->delivery_period?> дн.</p>
            </div>
        </div>
    </div>
</div>
