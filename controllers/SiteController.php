<?php

namespace app\controllers;

use Yii,
    yii\web\Controller,
    app\models\BoxberryApi;

class SiteController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $cities = BoxberryApi::ListCities();
        return $this->render('index', [
            'cities' => $cities
        ]);
    }
    
    public function actionCalculate($weight, $target, $orderSum)
    {
        $weight = floatval($weight);
        $target = floatval($target);
        $orderSum = floatval($orderSum);
        
        if ($weight > 0 && $target > 0 && $orderSum >= 0) {
            $price = BoxberryApi::DeliveryCosts($weight, $target, $orderSum);
            return $this->render('result', [
                'price' => $price
            ]);
        } else {
            throw new \yii\base\Exception("Invalid params");
        }
    }

}
